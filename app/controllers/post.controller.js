const postModel = require("../models/post.model");
const commentModel = require("../models/comment.model");
const mongoose = require("mongoose");

const createPost = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        userId,
        title,
        body
    } = req.body;

    // B2: Validate du lieu
    if (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    if (!body) {
        return res.status(400).json({
            message: "Yêu cầu body"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "Yêu cầu title"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newPost = {
            userId: userId,
            title: title,
            body: body
        }

        const result = await postModel.create(newPost);

        return res.status(201).json({
            message: "Tạo post thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllPosts = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let userId = req.query.userId;
    // B2: Validate du lieu
    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if(userId) {
        condition.userId = userId;
    }

    try {
        const result = await postModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách post thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getPostById = async (req, res) => {
    // B1: Thu thap du lieu
    const postId = req.params.postId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: "Post ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await postModel.findById(postId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin post thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin post"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updatePostById = async (req, res) => {
    // B1: Thu thap du lieu
    const postId = req.params.postId;

    const {
        userId,
        title,
        body
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: "Post ID không hợp lệ"
        })
    }

    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdatePost = {};
        if (userId) {
            newUpdatePost.userId = userId;
        }
        if (title) {
            newUpdatePost.title = title;
        }
        if (body) {
            newUpdatePost.body = body;
        }

        const result = await postModel.findByIdAndUpdate(postId, newUpdatePost);

        if (result) {
            const finalResult = await postModel.findById(postId);
            return res.status(200).json({
                message: "Update thông tin post thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin post"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deletePostById = async (req, res) => {
    // B1: Thu thap du lieu
    const postId = req.params.postId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: "Post ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await postModel.findByIdAndRemove(postId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin post thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin post"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getCommentsOfPost = async (req, res) => {
    // B1: Thu thap du lieu
    const postId = req.params.postId;
    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: "post ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    try {
        const result = await commentModel.find({postId: postId});

        return res.status(200).json({
            message: "Lấy danh sách comment thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createPost,
    getAllPosts,
    getPostById,
    updatePostById,
    deletePostById,
    getCommentsOfPost
}
