const userModel = require("../models/user.model");
const postModel = require("../models/post.model");
const albumModel = require("../models/album.model");
const todoModel = require("../models/todo.model");
const mongoose = require("mongoose");

const createUser = async (req, res) => {
    // B1: Thu thap du lieu
    const bodyData = req.body;

    // B2: Validate du lieu
    if (!bodyData.name) {
        return res.status(400).json({
            message: "Yêu cầu name"
        })
    }

    if (!bodyData.username) {
        return res.status(400).json({
            message: "Yêu cầu username"
        })
    }

    //tương tự cho các trường còn lại

    try {
        var newUser = {
            name: bodyData.name,
            username: bodyData.username,
            email: bodyData.email,
            address: {
              street: bodyData.address.street,
              suite: bodyData.address.suite,
              city: bodyData.address.city,
              zipcode: bodyData.address.zipcode,
              geo: {
                lat: bodyData.address.geo.lat,
                lng: bodyData.address.geo.lng
              }
            },
            phone: bodyData.phone,
            website: bodyData.website,
            company: {
              name: bodyData.company.name,
              catchPhrase: bodyData.company.catchPhrase,
              bs: bodyData.company.bs
            }
        }

        const result = await userModel.create(newUser);

        return res.status(201).json({
            message: "Tạo user thành công",
            data: result
        });
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        });
    }
}

const getAllUser = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await userModel.find();

        return res.status(200).json({
            message: "Lấy danh sách user thành công",
            data: result
        });
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        });
    }
}

const getUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findById(userId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin user thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;
    const bodyData = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateUser = {};
        if (bodyData.name) {
            newUpdateUser.name = bodyData.name;
        }
        if (bodyData.username) {
            newUpdateUser.username = bodyData.username;
        }
        if (bodyData.email) {
            newUpdateUser.email = bodyData.email;
        }
        if (bodyData.phone) {
            newUpdateUser.phone = bodyData.phone;
        }
        if (bodyData.website) {
            newUpdateUser.website = bodyData.website;
        }

        const result = await userModel.findByIdAndUpdate(userId, newUpdateUser);

        if (bodyData.address.street) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.street": bodyData.address.street}});
        }
        if (bodyData.address.suite) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.suite": bodyData.address.suite}});
        }
        if (bodyData.address.city) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.city": bodyData.address.city}});
        }
        if (bodyData.address.zipcode) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.zipcode": bodyData.address.zipcode}});
        }
        if (bodyData.address.geo.lat) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.geo.lat": bodyData.address.geo.lat}});
        }
        if (bodyData.address.geo.lng) {
            await userModel.findByIdAndUpdate(userId, {$set: {"address.geo.lng": bodyData.address.geo.lng}});
        }
        if (bodyData.company.name) {
            await userModel.findByIdAndUpdate(userId, {$set: {"company.name": bodyData.company.name}});
        }
        if (bodyData.company.catchPhrase) {
            await userModel.findByIdAndUpdate(userId, {$set: {"company.catchPhrase": bodyData.company.catchPhrase}});
        }
        if (bodyData.company.bs) {
            await userModel.findByIdAndUpdate(userId, {$set: {"company.bs": bodyData.company.bs}});
        }

        if (result) {
            const finalResult = await userModel.findById(userId);
            return res.status(200).json({
                message: "Update thông tin user thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findByIdAndRemove(userId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin user thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getPostsOfUser = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;
    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    try {
        const result = await postModel.find({userId: userId});

        return res.status(200).json({
            message: "Lấy danh sách post thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAlbumsOfUser = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;
    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    try {
        const result = await albumModel.find({userId: userId});

        return res.status(200).json({
            message: "Lấy danh sách album thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getTodosOfUser = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;
    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    try {
        const result = await todoModel.find({userId: userId});

        return res.status(200).json({
            message: "Lấy danh sách todo thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getPostsOfUser,
    getAlbumsOfUser,
    getTodosOfUser
}
