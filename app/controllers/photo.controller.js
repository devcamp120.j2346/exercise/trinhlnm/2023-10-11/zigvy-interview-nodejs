const photoModel = require("../models/photo.model");
const mongoose = require("mongoose");

const createPhoto = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        albumId,
        title,
        url,
        thumbnailUrl
    } = req.body;

    // B2: Validate du lieu
    if (typeof albumId != "string" || !mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "album ID không hợp lệ"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "Yêu cầu title"
        })
    }

    if (!url) {
        return res.status(400).json({
            message: "Yêu cầu url"
        })
    }

    if (!thumbnailUrl) {
        return res.status(400).json({
            message: "Yêu cầu thumbnailUrl"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newPhoto = {
            albumId: albumId,
            title: title,
            url: url,
            thumbnailUrl: thumbnailUrl
        }

        const result = await photoModel.create(newPhoto);

        return res.status(201).json({
            message: "Tạo photo thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllPhotos = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let albumId = req.query.albumId;
    // B2: Validate du lieu
    if (albumId && (typeof albumId != "string" || !mongoose.Types.ObjectId.isValid(albumId))) {
        return res.status(400).json({
            message: "album ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if(albumId) {
        condition.albumId = albumId;
    }

    try {
        const result = await photoModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách photo thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getPhotoById = async (req, res) => {
    // B1: Thu thap du lieu
    const photoId = req.params.photoId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: "Photo ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await photoModel.findById(photoId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin photo thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin photo"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updatePhotoById = async (req, res) => {
    // B1: Thu thap du lieu
    const photoId = req.params.photoId;

    const {
        albumId,
        title,
        url,
        thumbnailUrl
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: "Photo ID không hợp lệ"
        })
    }

    if (albumId && (typeof albumId != "string" || !mongoose.Types.ObjectId.isValid(albumId))) {
        return res.status(400).json({
            message: "album ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdatePhoto = {};
        if (albumId) {
            newUpdatePhoto.albumId = albumId;
        }
        if (title) {
            newUpdatePhoto.title = title;
        }
        if (url) {
            newUpdatePhoto.url = url;
        }
        if (thumbnailUrl) {
            newUpdatePhoto.thumbnailUrl = thumbnailUrl;
        }

        const result = await photoModel.findByIdAndUpdate(photoId, newUpdatePhoto);

        if (result) {
            const finalResult = await photoModel.findById(photoId);
            return res.status(200).json({
                message: "Update thông tin photo thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin photo"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deletePhotoById = async (req, res) => {
    // B1: Thu thap du lieu
    const photoId = req.params.photoId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: "Photo ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await photoModel.findByIdAndRemove(photoId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin photo thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin photo"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createPhoto,
    getAllPhotos,
    getPhotoById,
    updatePhotoById,
    deletePhotoById
}
