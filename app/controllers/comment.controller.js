const commentModel = require("../models/comment.model");
const mongoose = require("mongoose");

const createComment = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        postId,
        name,
        email,
        body
    } = req.body;

    // B2: Validate du lieu
    if (typeof postId != "string" || !mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            message: "post ID không hợp lệ"
        })
    }

    if (!name) {
        return res.status(400).json({
            message: "Yêu cầu name"
        })
    }

    if (!email) {
        return res.status(400).json({
            message: "Yêu cầu email"
        })
    }

    if (!body) {
        return res.status(400).json({
            message: "Yêu cầu body"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newComment = {
            postId: postId,
            name: name,
            email: email,
            body: body
        }

        const result = await commentModel.create(newComment);

        return res.status(201).json({
            message: "Tạo comment thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllComments = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let postId = req.query.postId;
    // B2: Validate du lieu
    if (postId && (typeof postId != "string" || !mongoose.Types.ObjectId.isValid(postId))) {
        return res.status(400).json({
            message: "post ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if(postId) {
        condition.postId = postId;
    }

    try {
        const result = await commentModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách comment thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getCommentById = async (req, res) => {
    // B1: Thu thap du lieu
    const commentId = req.params.commentId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: "Comment ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await commentModel.findById(commentId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin comment thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin comment"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateCommentById = async (req, res) => {
    // B1: Thu thap du lieu
    const commentId = req.params.commentId;

    const {
        postId,
        name,
        email,
        body
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: "Comment ID không hợp lệ"
        })
    }

    if (postId && (typeof postId != "string" || !mongoose.Types.ObjectId.isValid(postId))) {
        return res.status(400).json({
            message: "post ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateComment = {};
        if (postId) {
            newUpdateComment.postId = postId;
        }
        if (name) {
            newUpdateComment.name = name;
        }
        if (email) {
            newUpdateComment.email = email;
        }
        if (body) {
            newUpdateComment.body = body;
        }

        const result = await commentModel.findByIdAndUpdate(commentId, newUpdateComment);

        if (result) {
            const finalResult = await commentModel.findById(commentId);
            return res.status(200).json({
                message: "Update thông tin comment thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin comment"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteCommentById = async (req, res) => {
    // B1: Thu thap du lieu
    const commentId = req.params.commentId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            message: "Comment ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await commentModel.findByIdAndRemove(commentId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin comment thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin comment"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createComment,
    getAllComments,
    getCommentById,
    updateCommentById,
    deleteCommentById
}
