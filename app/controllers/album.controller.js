const albumModel = require("../models/album.model");
const photoModel = require("../models/photo.model");
const mongoose = require("mongoose");

const createAlbum = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        userId,
        title
    } = req.body;

    // B2: Validate du lieu
    if (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "Yêu cầu title"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newAlbum = {
            userId: userId,
            title: title
        }

        const result = await albumModel.create(newAlbum);

        return res.status(201).json({
            message: "Tạo album thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllAlbums = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let userId = req.query.userId;
    // B2: Validate du lieu
    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if(userId) {
        condition.userId = userId;
    }

    try {
        const result = await albumModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách album thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAlbumById = async (req, res) => {
    // B1: Thu thap du lieu
    const albumId = req.params.albumId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "Album ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await albumModel.findById(albumId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin album thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin album"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateAlbumById = async (req, res) => {
    // B1: Thu thap du lieu
    const albumId = req.params.albumId;

    const {
        userId,
        title
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "album ID không hợp lệ"
        })
    }

    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateAlbum = {};
        if (userId) {
            newUpdateAlbum.userId = userId;
        }
        if (title) {
            newUpdateAlbum.title = title;
        }

        const result = await albumModel.findByIdAndUpdate(albumId, newUpdateAlbum);

        if (result) {
            const finalResult = await albumModel.findById(albumId);
            return res.status(200).json({
                message: "Update thông tin album thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin album"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteAlbumById = async (req, res) => {
    // B1: Thu thap du lieu
    const albumId = req.params.albumId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "Album ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await albumModel.findByIdAndRemove(albumId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin album thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin album"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}


const getPhotosOfAlbum = async (req, res) => {
    // B1: Thu thap du lieu
    const albumId = req.params.albumId;
    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "Album ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    try {
        const result = await photoModel.find({albumId: albumId});

        return res.status(200).json({
            message: "Lấy danh sách photo thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createAlbum,
    getAllAlbums,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getPhotosOfAlbum
}
