const todoModel = require("../models/todo.model");
const mongoose = require("mongoose");

const createTodo = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        userId,
        title,
        completed
    } = req.body;

    // B2: Validate du lieu
    if (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "Yêu cầu title"
        })
    }

    if (!completed) {
        return res.status(400).json({
            message: "Yêu cầu completed"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newTodo = {
            userId: userId,
            title: title,
            completed: completed
        }

        const result = await todoModel.create(newTodo);

        return res.status(201).json({
            message: "Tạo todo thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllTodos = async (req, res) => {
    let condition = {};
    // B1: Thu thap du lieu
    let userId = req.query.userId;
    // B2: Validate du lieu
    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }
    // B3: Xu ly du lieu
    if(userId) {
        condition.userId = userId;
    }

    try {
        const result = await todoModel.find(condition);

        return res.status(200).json({
            message: "Lấy danh sách todo thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getTodoById = async (req, res) => {
    // B1: Thu thap du lieu
    const todoId = req.params.todoId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: "Todo ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await todoModel.findById(todoId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin todo thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin todo"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateTodoById = async (req, res) => {
    // B1: Thu thap du lieu
    const todoId = req.params.todoId;

    const {
        userId,
        title,
        completed
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: "Todo ID không hợp lệ"
        })
    }

    if (userId && (typeof userId != "string" || !mongoose.Types.ObjectId.isValid(userId))) {
        return res.status(400).json({
            message: "user ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateTodo = {};
        if (userId) {
            newUpdateTodo.userId = userId;
        }
        if (title) {
            newUpdateTodo.title = title;
        }
        if (completed) {
            newUpdateTodo.completed = completed;
        }

        const result = await todoModel.findByIdAndUpdate(todoId, newUpdateTodo);

        if (result) {
            const finalResult = await todoModel.findById(todoId);
            return res.status(200).json({
                message: "Update thông tin todo thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin todo"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteTodoById = async (req, res) => {
    // B1: Thu thap du lieu
    const todoId = req.params.todoId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return res.status(400).json({
            message: "Todo ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await todoModel.findByIdAndRemove(todoId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin todo thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin todo"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createTodo,
    getAllTodos,
    getTodoById,
    updateTodoById,
    deleteTodoById
}
