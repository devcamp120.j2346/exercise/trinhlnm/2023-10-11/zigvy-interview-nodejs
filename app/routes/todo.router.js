const express = require("express");

const router = express.Router();

const {
    createTodo,
    getAllTodos,
    getTodoById,
    updateTodoById,
    deleteTodoById
} = require("../controllers/todo.controller");

router.get("/", getAllTodos);

router.post("/", createTodo);

router.get("/:todoId", getTodoById);

router.put("/:todoId", updateTodoById);

router.delete("/:todoId", deleteTodoById);

module.exports = router;

