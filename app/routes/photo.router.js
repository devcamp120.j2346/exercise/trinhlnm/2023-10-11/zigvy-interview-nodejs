const express = require("express");

const router = express.Router();

const {
    createPhoto,
    getAllPhotos,
    getPhotoById,
    updatePhotoById,
    deletePhotoById
} = require("../controllers/photo.controller");

router.get("/", getAllPhotos);

router.post("/", createPhoto);

router.get("/:photoId", getPhotoById);

router.put("/:photoId", updatePhotoById);

router.delete("/:photoId", deletePhotoById);

module.exports = router;

