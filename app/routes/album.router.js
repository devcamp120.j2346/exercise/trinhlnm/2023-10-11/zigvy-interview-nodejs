const express = require("express");

const router = express.Router();

const {
    createAlbum,
    getAllAlbums,
    getAlbumById,
    updateAlbumById,
    deleteAlbumById,
    getPhotosOfAlbum
} = require("../controllers/album.controller");

router.get("/", getAllAlbums);

router.post("/", createAlbum);

router.get("/:albumId", getAlbumById);

router.get("/:albumId/photos", getPhotosOfAlbum);

router.put("/:albumId", updateAlbumById);

router.delete("/:albumId", deleteAlbumById);

module.exports = router;

