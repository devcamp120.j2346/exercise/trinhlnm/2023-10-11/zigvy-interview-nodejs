const express = require("express");

const router = express.Router();

const {
    createPost,
    getAllPosts,
    getPostById,
    updatePostById,
    deletePostById,
    getCommentsOfPost
} = require("../controllers/Post.controller");

router.get("/", getAllPosts);

router.post("/", createPost);

router.get("/:postId", getPostById);

router.get("/:postId/comments", getCommentsOfPost);

router.put("/:postId", updatePostById);

router.delete("/:postId", deletePostById);

module.exports = router;

