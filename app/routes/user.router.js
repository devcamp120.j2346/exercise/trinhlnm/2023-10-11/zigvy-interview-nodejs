const express = require("express");

const router = express.Router();

const {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getPostsOfUser,
    getAlbumsOfUser,
    getTodosOfUser
} = require("../controllers/user.controller");

router.get("/", getAllUser);

router.post("/", createUser);

router.get("/:userId", getUserById);

router.get("/:userId/posts", getPostsOfUser);

router.get("/:userId/albums", getAlbumsOfUser);

router.get("/:userId/todos", getTodosOfUser);

router.put("/:userId", updateUserById);

router.delete("/:userId", deleteUserById);

module.exports = router;

