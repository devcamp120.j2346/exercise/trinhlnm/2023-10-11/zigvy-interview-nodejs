const express = require("express");

const router = express.Router();

const {
    createComment,
    getAllComments,
    getCommentById,
    updateCommentById,
    deleteCommentById
} = require("../controllers/comment.controller");

router.get("/", getAllComments);

router.post("/", createComment);

router.get("/:commentId", getCommentById);

router.put("/:commentId", updateCommentById);

router.delete("/:commentId", deleteCommentById);

module.exports = router;

