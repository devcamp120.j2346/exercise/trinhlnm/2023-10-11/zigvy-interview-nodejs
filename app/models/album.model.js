const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const albumSchema = new Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "User"
    },
    title: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Album", albumSchema);
