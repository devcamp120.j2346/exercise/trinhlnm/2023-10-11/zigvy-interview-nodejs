const express = require("express");
const path = require("path");
const mongoose = require('mongoose');

const userRouter = require("./app/routes/user.router");
const postRouter = require("./app/routes/post.router");
const commentRouter = require("./app/routes/comment.router");
const albumRouter = require("./app/routes/album.router");
const photoRouter = require("./app/routes/photo.router");
const todoRouter = require("./app/routes/todo.router");

const app = express();

const port = 8000;

app.use(express.json());

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Zigvy_Interview")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

app.use("/users", userRouter);
app.use("/posts", postRouter);
app.use("/comments", commentRouter);
app.use("/albums", albumRouter);
app.use("/photos", photoRouter);
app.use("/todos", todoRouter);

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});